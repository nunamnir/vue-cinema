import { createStore } from 'vuex'
import axios from 'axios';

export default createStore({
  state: {
    filmsdata: [],
    sessionsdata: [],
    currentFilmSession: {},
    genreSort: 0,
  },
  mutations: {
    async getAllFilms(state){
      if(state.filmsdata.length === 0) {
        axios.get('https://cinema-api-test.y-media.io/v1/movies').then(resp => {
          state.filmsdata = { ...resp.data };
        });
      } else return
    },
    async getAllSessions(state){
      if(state.sessionsdata.length === 0) {
        axios.get('https://cinema-api-test.y-media.io/v1/movieShows').then(resp => {
          state.sessionsdata = { ...resp.data };
        });
      } else return
    },
    async getFilmSessionById(state, id) {
      axios.get(`https://cinema-api-test.y-media.io/v1/movieShows?movie_id=${id}`).then(resp => {
        state.currentFilmSession = { ...resp.data };
      });
    },
    async getFilmsByGenre(state, genreId) {
      axios.get(`https://cinema-api-test.y-media.io/v1/movieShows?genres=${genreId}`).then(resp => {
        state.filmsdata = { ...resp.data };
      });
    },
  },
  actions: {
    getAllFilms: (context) => {
      context.commit('getAllFilms');
    },
    getAllSessions: (context) => {      
      context.commit('getAllSessions');
    },
    getFilmSessionById: (context, arg) => {
      context.commit('getFilmSessionById', arg);
    },
    getFilmsByGenre: (context, arg) => {
      context.commit('getFilmsByGenre', arg);
    },
  },
  getters: {
    getAllFilms(state) {
      return state.filmsdata;
    },
    getAllSessions(state) {
      return state.sessionsdata;
    },
    getCurrentFilmSession(state) {
      return state.currentFilmSession;
    },
  },
})
